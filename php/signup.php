<?php
    session_start();
    include_once "config.php";
    $fname = mysqli_real_escape_string($conn, $_POST['fname']);
    $lname = mysqli_real_escape_string($conn, $_POST['lname']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    if(!empty($fname) && !empty($lname) && !empty($email) && !empty($password)){
        // validation email
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            // si mail existe dans la bdd
            $sql = mysqli_query($conn, "SELECT email FROM users WHERE email = '{$email}'");
            if(mysqli_num_rows($sql) > 0){
                echo " $email - Cette adresse est déjà enregistrée !";
            }else{
                // verification img utilisateur
                if(isset($_FILES['image'])){
                    $img_name = $_FILES['image']['name'];
                    $img_type = $_FILES['image']['type'];
                    $tmp_name = $_FILES['image']['tmp_name'];

                    //recupérer extension image
                    $img_explode = explode('.', $img_name);
                    $img_ext = end($img_explode);
                    $extensions = ['png', 'jpeg', 'jpg', 'gif'];
                    //verif extensions image
                    if(in_array($img_ext, $extensions) === true){
                        $time = time();
                        $new_img_name = $time.$img_name;

                        if(move_uploaded_file($tmp_name, "images/".$new_img_name )){
                            $status = "Connecté";
                            $random_id = rand(time(), 10000000);

                            // insertion données utilisateur dans la bdd
                            $sql2 = mysqli_query($conn, "INSERT INTO users (unique_id, fname, lname, email, password,
                   img, status) VALUES ({$random_id}, '{$fname}', '{$lname}', '{$email}', '{$password}', '{$new_img_name}',
                                        '{$status}')");

                            if($sql2){ // si les données sont insérées
                                $sql3 = mysqli_query($conn, "SELECT * FROM users WHERE email = '{$email}'");
                                if(mysqli_num_rows($sql3) > 0){
                                    $row = mysqli_fetch_assoc($sql3);
                                    $_SESSION['unique_id'] = $row['unique_id'];
                                    // association d'un id unique par utilisateur et par session
                                    echo "success";
                                }
                            }else{
                                echo "Erreur inconnue ! ";
                            }
                        }
                    }else{
                        echo "Veuillez choisir un format d'image valide - .jpg, .jpeg, .png, .gif !";
                    }
                }else{
                    echo "Veuillez choisir une image";
                }
            }
        }else{
            echo "$email n'est pas une adresse valide ! ";
        }
    }else{
        echo "Tous les champs doivent être renseignés ! ";
    }
