<?php include_once "header.php"; ?>
<body>
<div class="wrapper">
    <section class="form login">
        <header>Chat PHP | Connexion</header>
        <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
            <div class="error-text"></div>
            <div class="field input">
                <label>Email</label>
                <input type="text" name="email" placeholder="Entrez votre email" required>
            </div>
            <div class="field input">
                <label>Mot de passe</label>
                <input type="password" name="password" placeholder="Entrez votre mot de passe" required>
                <i class="fas fa-eye"></i>
            </div>
            <div class="field button">
                <input type="submit" name="submit" value="Accédez au chat">
            </div>
        </form>
        <div class="link">Pas encore enregistré ? <a href="index.php">Enregistrez-vous</a></div>
    </section>
</div>

<script src="javascript/pass-show-hide.js"></script>
<script src="javascript/login.js"></script>

</body>
</html>