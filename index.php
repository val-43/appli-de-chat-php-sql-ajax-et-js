<?php include_once "header.php"; ?>
<body>
<div class="wrapper">
    <section class="form signup">
        <header>Chat PHP | Inscription </header>
        <form action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
            <div class="error-text"></div>
            <div class="name-details">
                <div class="field input">
                    <label>Prénom</label>
                    <input type="text" name="fname" placeholder="Prénom" required>
                </div>
                <div class="field input">
                    <label>Nom</label>
                    <input type="text" name="lname" placeholder="Nom" required>
                </div>
            </div>
            <div class="field input">
                <label>Email</label>
                <input type="text" name="email" placeholder="Entrez votre email" required>
            </div>
            <div class="field input">
                <label>Mot de passe</label>
                <input type="password" name="password" placeholder="Entrez votre mot de passe" required>
                <i class="fas fa-eye"></i>
            </div>
            <div class="field image">
                <label>Choisissez une image de profil</label>
                <input type="file" name="image" accept="image/x-png,image/gif,image/jpeg,image/jpg" required>
            </div>
            <div class="field button">
                <input type="submit" name="submit" value="Accédez au chat">
            </div>
        </form>
        <div class="link">Déjà enregistré ? <a href="login.php">Connectez-vous</a></div>
    </section>
</div>

<script src="javascript/pass-show-hide.js"></script>
<script src="javascript/signup.js"></script>

</body>
</html>